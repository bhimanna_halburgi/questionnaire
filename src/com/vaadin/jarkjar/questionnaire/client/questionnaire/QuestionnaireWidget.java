package com.vaadin.jarkjar.questionnaire.client.questionnaire;

import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Questionnaire client widget
 * 
 * @author Jarkko Järvinen
 * 
 */
public class QuestionnaireWidget extends VerticalPanel {

    public static final String CLASSNAME = "questionnaire";

    public QuestionnaireWidget() {
        setStyleName(CLASSNAME);
    }
}