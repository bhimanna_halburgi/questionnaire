package com.vaadin.jarkjar.questionnaire.client.questionnaire;


public class QuestionnaireState extends
        com.vaadin.shared.AbstractComponentState {

    private QuestionSet questionSet;

    /**
     * @return the questionSet
     */
    public QuestionSet getQuestionSet() {
        return questionSet;
    }

    /**
     * @param questionSet
     *            the questionSet to set
     */
    public void setQuestionSet(QuestionSet questionSet) {
        this.questionSet = questionSet;
    }
}