package com.vaadin.jarkjar.questionnaire;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.List;

import com.vaadin.jarkjar.questionnaire.client.questionnaire.Question;
import com.vaadin.jarkjar.questionnaire.client.questionnaire.Question.QuestionType;
import com.vaadin.jarkjar.questionnaire.client.questionnaire.QuestionSet;
import com.vaadin.jarkjar.questionnaire.client.questionnaire.QuestionnaireServerRpc;
import com.vaadin.jarkjar.questionnaire.client.questionnaire.QuestionnaireState;
import com.vaadin.jarkjar.questionnaire.client.questionnaire.UserAnswer;
import com.vaadin.ui.Component;
import com.vaadin.util.ReflectTools;

/**
 * Questionnaire component to create questionnaires.
 * 
 * @author Jarkko Järvinen
 * 
 */
public class Questionnaire extends com.vaadin.ui.AbstractComponent {

    /**
     * Contain user answers. Will be filled when submit button has been clicked.
     */
    private List<UserAnswer> userAnswers;

    private QuestionnaireServerRpc rpc = new QuestionnaireServerRpc() {
        @Override
        public void submitButtonClicked() {
            // Simulates a button click, notifying all server-side listeners.
            // No action is taken if the questionnaire is disabled.
            if (isEnabled() && !isReadOnly()) {
                // Fires a click event to all listeners without any event
                // details.
                fireEvent(new SubmitButtonClickEvent(Questionnaire.this));
            }
        }

        @Override
        public void setUserAnswers(List<UserAnswer> userAnswers) {
            Questionnaire.this.userAnswers = userAnswers;
        }
    };

    /**
     * Submit button click event. This event is thrown when the submit button is
     * clicked.
     * 
     * @author Jarkko Järvinen
     * 
     */
    public class SubmitButtonClickEvent extends Event {
        public SubmitButtonClickEvent(Component source) {
            super(source);
        }
    }

    /**
     * Interface for listening for a (@link SubmitButtonClickEvent) fired by a
     * (@link Component)
     * 
     * @author Jarkko Järvinen
     * 
     */
    public interface SubmitButtonClickListener extends Serializable {
        public static final Method BUTTON_CLICK_METHOD = ReflectTools
                .findMethod(SubmitButtonClickListener.class, "buttonClick",
                        SubmitButtonClickEvent.class);

        public void buttonClick(SubmitButtonClickEvent event);
    }

    /**
     * Adds the button click listener.
     * 
     * @param listener
     *            the Listener to be added.
     */
    public void addClickListener(SubmitButtonClickListener listener) {
        addListener(SubmitButtonClickEvent.class, listener,
                SubmitButtonClickListener.BUTTON_CLICK_METHOD);
    }

    /**
     * Removes the button click listener.
     * 
     * @param listener
     *            the Listener to be removed.
     */
    public void removeClickListener(SubmitButtonClickListener listener) {
        removeListener(SubmitButtonClickEvent.class, listener,
                SubmitButtonClickListener.BUTTON_CLICK_METHOD);
    }

    public Questionnaire() {
        registerRpc(rpc);
    }

    /**
     * Set questionnaire data to component
     * 
     * @param questionSet
     */
    public void setDataSource(QuestionSet questionSet) {
        if (questionSet == null) {
            throw new IllegalArgumentException("QuestionSet cannot be null");
        } else if (questionSet.getQuestions().size() == 0) {
            throw new IllegalArgumentException(
                    "QuestionSet had to contain at least one question");
        }
        for (Question question : questionSet.getQuestions()) {
            if (question.getType() == QuestionType.CHECKBOX
                    || question.getType() == QuestionType.RADIOBUTTON) {
                if (question.getAnswers().size() < 2) {
                    throw new IllegalArgumentException(
                            "Question with type checkbox or radiobutton need at least two answers!");
                }
            }
        }
        this.getState().setQuestionSet(questionSet);
        markAsDirty();
    }

    @Override
    public QuestionnaireState getState() {
        return (QuestionnaireState) super.getState();
    }

    /**
     * Return all user answers. User answers will be available after submit
     * button click.
     * 
     * @return
     */
    public List<UserAnswer> getUserAnswers() {
        return this.userAnswers;
    }
}
